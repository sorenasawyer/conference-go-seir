# Select the base image that is best for our application
FROM python:3
# Downloading python because we are using Django

# Install any operating system junk
# Nothing is here because we are using Python

# Set the working directory to copy stuff to
WORKDIR /app
# This is a normal place 

# Copy all the code from the local directory into the image
COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
# These are specifically what we want to copy for our project

# Install any language dependencies
RUN pip install -r requirements.txt

# Set the command to run the application
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
# This command specifically allows for Insomnia to work with gunicorn